// dependencies
import React, { useState } from "react";

// files import
import { selectTemplateData } from "utils/dummyData";
import styles from "styles/components/SideBarContent.module.scss";
import {
  IS_BUTTON_VISIBLE,
  IS_HEADLINE_LIGHT,
  SELECTED_TEMPLATE_OPTION,
  VIDEO_URL,
} from "utils/hooks/sharedState";
import { HeadLineInput } from "./templates/HeadLine";
import { DescriptionInput } from "./templates/Description";
import { ButtonInput } from "./templates/Button";
import { Switch } from "@mui/material";

const SideBarContent = ({ activeCount, onManageCountStep }) => {
  const [templateData, setTemplateData] = useState(selectTemplateData);
  const [buttonTextValue, setButtonTextValue] = useState("");
  const [selectCardValue, setSelectCardValue] = useState("");
  const [selectedTemplate, setSelectedTemplate] =
    SELECTED_TEMPLATE_OPTION.useSharedState();

  const [videoURL, setVideoURL] = VIDEO_URL.useSharedState();
  const [isHeadlineLight, setIsHeadlineLight] =
    IS_HEADLINE_LIGHT.useSharedState();
  const [isButtonVisible, setIsButtonVisible] =
    IS_BUTTON_VISIBLE.useSharedState();

  const SelectTemplate = () => {
    return (
      <div className={styles.selectTemplateContainer}>
        <span>Select Your Design Template</span>
        {templateData?.map((template, tIndex) => {
          return (
            <div
              key={tIndex}
              className={styles.option}
              data-is-active-option={tIndex === selectedTemplate}
              onClick={() => setSelectedTemplate(tIndex)}
            >
              <img src={template.img} alt="" />
              <span>{template.name}</span>
            </div>
          );
        })}
        <span className={styles.note}>
          <img src="/assets/images/picker.svg" alt="" />
          Need to change your church’s logo or colors?
        </span>
        <button onClick={() => onManageCountStep(2)}>Next</button>
      </div>
    );
  };

  const PageDesign = () => {
    return (
      <div className={styles.pageDesignContainer}>
        <span className={styles.title}>Page title</span>
        <HeadLineInput />

        <div className={styles.textColorContainer}>
          <span className={styles.textColor}>Headline text color</span>
          <div className={styles.switchContainer}>
            <Switch
              checked={isHeadlineLight}
              onChange={(e) => setIsHeadlineLight(e.target.checked)}
            />
            <span className={styles.switch}>
              {isHeadlineLight ? "Light" : "Dark"}
            </span>
          </div>
        </div>

        <div className={styles.descriptionContainer}>
          <span className={styles.description}>Page description</span>
          <DescriptionInput />
        </div>

        <div className={styles.buttonContainer}>
          <div className={styles.designContainer}>
            <span className={styles.buttonDesign}>Button design</span>
            <div className={styles.enabledContainer}>
              <Switch
                checked={isButtonVisible}
                onClick={() => setIsButtonVisible(!isButtonVisible)}
              />
              <span
                onClick={() => console.log("dbhfjhf")}
                className={styles.enabled}
              >
                {isButtonVisible ? "Enabled" : "Disabled"}
              </span>
            </div>
          </div>

          {isButtonVisible && (
            <>
              <ButtonInput />
              <div className={styles.linkContainer}>
                <span className={styles.linkTitle}>Link to: </span>
                <div className={styles.inputContainer}>
                  <input type="checkbox" />
                  <span>URL</span>
                </div>
                <div className={styles.inputContainer}>
                  <input type="checkbox" />
                  <span>CONNECT CARD</span>
                </div>
                <div className={styles.inputContainer}>
                  <input type="checkbox" />
                  <span>VIDEO PAGE</span>
                </div>
              </div>

              <div className={styles.connectContainer}>
                <span className={styles.connectTitle}>Connect Card</span>
                <div className={styles.radioContainer}>
                  <div className={styles.popupContainer}>
                    <input type="radio" />
                    <span>Open in pop up</span>
                  </div>
                  <div className={styles.popupContainer}>
                    <input type="radio" />
                    <span>Display on page</span>
                  </div>
                </div>

                <div className={styles.selectConnect}>
                  <select onChange={(e) => setSelectCardValue(e.target.value)}>
                    <option>Select Connect Card</option>
                  </select>
                </div>
              </div>
            </>
          )}
        </div>
        <button onClick={() => onManageCountStep(3)}>Next</button>
      </div>
    );
  };

  const CreateVideo = () => {
    return (
      <div className={styles.createVideoContainer}>
        <span>Add your Video</span>
        <div className={styles.options}>
          <div className={styles.option}>
            <span>Record</span>
            <img src="/assets/icons/video.svg" alt="" />
          </div>
          <div className={styles.option} data-is-active-option={true}>
            <span>YouTube or Vimeo</span>
            <img src="/assets/icons/youtube.svg" alt="" />
          </div>
          <div className={styles.option}>
            <span>upload</span>
            <img src="/assets/icons/add.svg" alt="" />
          </div>
        </div>
        <span className={styles.title}>Video URL</span>
        <input
          placeholder="Paste URL here"
          onChange={(e) => setVideoURL(e.target.value)}
          className={styles.headlineInput}
          value={videoURL}
        />
        <button>Publish (Coming Soon)</button>
      </div>
    );
  };

  return (
    <div className={styles.mainContainer}>
      {activeCount === 1 && <SelectTemplate />}
      {activeCount === 2 && <PageDesign />}
      {activeCount === 3 && <CreateVideo />}
    </div>
  );
};

export default SideBarContent;
