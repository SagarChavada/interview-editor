// dependencies
import React from "react";

// files import
import styles from "styles/components/ActiveTab.module.scss";

const ActiveTab = ({ activeCount }) => {
  return (
    <div className={styles.mainContainer}>
      <div className={styles.tab} data-is-active-tab={activeCount === 1}>
        {activeCount == 2 || activeCount == 3 ? (
          <div
            className={`${
              activeCount == 2 || activeCount == 3 ? styles.active : styles.step
            }`}
          >
            <img src="/assets/icons/check.svg" />
          </div>
        ) : (
          <div className={styles.step}>1</div>
        )}
        {activeCount == 2 || activeCount == 3 ? (
          <div className={styles.divider} />
        ) : (
          <span>Select Template</span>
        )}
      </div>
      <div className={styles.tab} data-is-active-tab={activeCount === 2}>
        {activeCount == 3 ? (
          <>
            <div
              className={`${activeCount == 3 ? styles.active : styles.step}`}
            >
              <img src="/assets/icons/check.svg" />
            </div>
            <div className={styles.divider} />
          </>
        ) : (
          <>
            <div className={styles.divider} />
            <div className={styles.step}>2</div>
          </>
        )}

        <span>Page Design</span>
      </div>
      <div className={styles.tab} data-is-active-tab={activeCount === 3}>
        <div className={styles.divider} />
        <div className={styles.step}>3</div>
        <span>Create your Video</span>
      </div>
    </div>
  );
};

export default ActiveTab;
