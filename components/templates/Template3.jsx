import React from "react";

// files import
import styles from "styles/components/Template.module.scss";
import { HeadLine } from "./HeadLine";
import { Description } from "./Description";
import VideoPlayer from "./VideoPlayer";
import { Button } from "./Button";

const Template3 = ({ templateData }) => {
  return (
    <div className={styles.mainTemplateContainer}>
      <div className={styles.highLightContainer}>
        <div className={styles.highLightBanner1} />
        <div className={styles.highLightBanner2} />
      </div>
      <HeadLine isLight={templateData?.isheadlinelight} text={templateData?.headline} />
      <div className={styles.videoContainer}>
        <VideoPlayer url={templateData?.videourl} />
      </div>
      <div className={styles.messageContainer}>
        <Description text={templateData?.desc} />
        <Button isVisible={templateData?.isbuttonvisible} text={templateData?.buttonText} />
      </div>
    </div>
  );
};

export default Template3;
