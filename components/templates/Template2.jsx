import React from "react";

// files import
import styles from "styles/components/Template.module.scss";
import { HeadLine } from "./HeadLine";
import { Description } from "./Description";
import VideoPlayer from "./VideoPlayer";
import { Button } from "./Button";

const Template2 = ({ templateData }) => {
  return (
    <>
      <div className={styles.mainTemplateContainer}>
        <div className={styles.bubbleContainer}>
          <img className={styles.bubble1} src="/assets/images/bubble1.svg" />
          <img className={styles.bubble2} src="/assets/images/bubble2.svg" />
        </div>

        <HeadLine isLight={templateData?.isheadlinelight} text={templateData?.headline} />
        <div className={styles.videoContainer}>
          <VideoPlayer url={templateData?.videourl} />
        </div>
        <div className={styles.bubble3Container}>
          <img className={styles.bubble2} src="/assets/images/bubble3.svg" />
        </div>

        <div className={styles.messageContainer}>
          <Description text={templateData?.desc} />
          <Button isVisible={templateData?.isbuttonvisible} text={templateData?.buttonText} />
        </div>
      </div>
    </>
  );
};

export default Template2;
