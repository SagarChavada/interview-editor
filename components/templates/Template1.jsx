// dependencies
import React from "react";

// files import
import styles from "styles/components/Template.module.scss";
import { Description } from "./Description";
import { HeadLine } from "./HeadLine";
import { Button } from "./Button";
import VideoPlayer from "./VideoPlayer";

const Template1 = ({ templateData }) => {
  return (
    <>
      <div className={styles.mainTemplateContainer}>
        <img
          className={styles.template1Bg}
          src="/assets/images/templateBg1.svg"
        />
        <HeadLine isLight={templateData?.isheadlinelight} text={templateData?.headline} inverted={true} />
        <div className={styles.videoContainer}>
          <VideoPlayer url={templateData?.videourl} />
        </div>
        <div className={styles.messageContainer}>
          <Description text={templateData?.desc} />
          <Button isVisible={templateData?.isbuttonvisible} text={templateData?.buttonText} />
        </div>
      </div>
    </>
  );
};

export default Template1;
