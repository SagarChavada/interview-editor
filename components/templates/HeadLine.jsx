// dependencies
import React from "react";

// files import
import styles from "styles/components/Template.module.scss";
import { IS_HEADLINE_LIGHT, TEMPLATE_HEADLINE } from "utils/hooks/sharedState";

export const HeadLine = ({text, isLight}) => {
  const [headlineText] = TEMPLATE_HEADLINE.useSharedState();
  const [isHeadlineLight] =IS_HEADLINE_LIGHT.useSharedState();

  const manageColor = isLight ?? isHeadlineLight

  return (
    <div className={styles.headline} style={{color: manageColor && 'white'}}>
      {text ? text : headlineText}
    </div>
  );
};

export const HeadLineInput = () => {
  const [headlineText, setHeadlineText] = TEMPLATE_HEADLINE.useSharedState();

  return (
    <input
      placeholder="Headline"
      onChange={(e) => setHeadlineText(e.target.value)}
      className={styles.headlineInput}
      value={headlineText}
    />
  );
};
