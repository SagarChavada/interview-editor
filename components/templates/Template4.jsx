import React from "react";

// files import
import styles from "styles/components/Template.module.scss";
import { HeadLine } from "./HeadLine";
import { Description } from "./Description";
import VideoPlayer from "./VideoPlayer";
import { Button } from "./Button";

const Template4 = ({ templateData }) => {
  return (
    <div className={styles.mainTemplateContainer}>
      <div className={styles.highLight4} />
      <HeadLine isLight={templateData?.isheadlinelight} text={templateData?.headline} />
      <div className={styles.videoContainer}>
        <VideoPlayer url={templateData?.videourl} />
      </div>
      <div className={styles.messageContainer}>
        <Description text={templateData?.desc} />
        <Button isVisible={templateData?.isbuttonvisible} inverted={true} text={templateData?.buttonText} />
      </div>
    </div>
  );
};

export default Template4;
