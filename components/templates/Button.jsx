// dependencies
import React from "react";

// files import
import styles from "styles/components/Template.module.scss";
import { BUTTON_TEXT, IS_BUTTON_VISIBLE } from "utils/hooks/sharedState";

export const Button = ({text, inverted, isVisible}) => {
  const [buttonText] = BUTTON_TEXT.useSharedState();
  const [isButtonVisible] = IS_BUTTON_VISIBLE.useSharedState();

  const buttonVisibility = isVisible ?? isButtonVisible

  console.log('buttonVisibility',buttonVisibility, isVisible)

  return buttonVisibility && (
    <button className={inverted && styles.buttonInverted} onClick={() => alert('Coming soon !!!!')}>
      {text ? text : buttonText}
    </button>
  );
};

export const ButtonInput = () => {
  const [buttonText, setButtonText] = BUTTON_TEXT.useSharedState();

  return (
    <input
      placeholder="Button Text"
      onChange={(e) => setButtonText(e.target.value)}
      value={buttonText}
    />
  );
};
