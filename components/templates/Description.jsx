// dependencies
import React from "react";

// files import
import styles from "styles/components/Template.module.scss";
import { TEMPLATE_DESC } from "utils/hooks/sharedState";

export const Description = ({text}) => {
  const [descText,] = TEMPLATE_DESC.useSharedState();

  return (
    <span className={styles.message}>
      {text ? text : descText}
    </span>
  );
};

export const DescriptionInput = () => {
  const [descText, setDescText] = TEMPLATE_DESC.useSharedState();

  return (
    <textarea
      onChange={(e) => setDescText(e.target.value)}
      rows="10"
      value={descText}
      placeholder="Add your message here..."
      className={styles.textarea}
    />
  );
};
