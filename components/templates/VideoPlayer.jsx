import React from "react";
import ReactPlayer from "react-player";
import { VIDEO_URL } from "utils/hooks/sharedState";
import styles from "styles/components/Template.module.scss";

const VideoPlayer = ({url}) => {
  const [videoURL] = VIDEO_URL.useSharedState();
  
  return (
    <div className={styles.videoSubContainer}>
      {(url || videoURL) ? (
        <ReactPlayer
          url={url ? url : videoURL} 
          height="100%"
          width="100%"  
          playing={true}
          />
      ) : (
        <img src="/assets/icons/video_lg.svg" alt="" />
      )}
    </div>
  );
};

export default VideoPlayer;
