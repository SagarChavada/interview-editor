// dependencies
import React from "react";
import { useRouter } from "next/router";
import Swal from 'sweetalert2/dist/sweetalert2.js'

// files import
import styles from "styles/components/Layout.module.scss";
import { BUTTON_TEXT, IS_BUTTON_VISIBLE, IS_HEADLINE_LIGHT, SELECTED_TEMPLATE_OPTION, TEMPLATE_DESC, TEMPLATE_HEADLINE, VIDEO_URL } from "utils/hooks/sharedState";
import Template1 from "./templates/Template1";
import Template2 from "./templates/Template2";
import Template3 from "./templates/Template3";
import Template4 from "./templates/Template4";

const Layout = () => {
  const [selectedTemplate, setSelectedTemplate] =
    SELECTED_TEMPLATE_OPTION.useSharedState();
  const [headline] = TEMPLATE_HEADLINE.useSharedState();
  const [descText] = TEMPLATE_DESC.useSharedState();
  const [buttonText] = BUTTON_TEXT.useSharedState();
  const [videoURL] = VIDEO_URL.useSharedState();
  const [isHeadlineLight] = IS_HEADLINE_LIGHT.useSharedState();
  const [isButtonvisible] = IS_BUTTON_VISIBLE.useSharedState();

  const router = useRouter();

  const onPreviewClick = () => {
    router.push({
      pathname: '/videomessage',
      query: {
        template: selectedTemplate,
        headline: headline,
        desc: descText,
        videourl: videoURL,
        buttonText: buttonText,
        isbuttonvisible: isButtonvisible,
        isheadlinelight: isHeadlineLight
      }
    })
  }

  const onLinkClick = () => {
    const share_link = `${process.env.NEXT_PUBLIC_URL}/videomessage?template=${selectedTemplate}&headline=${headline}&desc=${descText}&buttonText=${buttonText}&videourl=${videoURL}&isbuttonvisible=${isButtonvisible}&isheadlinelight=${isHeadlineLight}`
    const split_string = share_link?.split(" ")
    
    navigator.clipboard.writeText(split_string?.join("%20"))
      .then(res => Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Link Copied Successfully',
        showConfirmButton: false,
        timer: 2000
      }))
  }

  return (
    <div className={styles.mainContainer}>
      <div className={styles.row}>
        <div className={styles.leftSection}>
          <img src="/assets/icons/mobile.svg" alt="" />
          <img src="/assets/icons/tablet.svg" alt="" />
          <img src="/assets/icons/desktop.svg" alt="" />
        </div>
        <div className={styles.rightSection}>
          <img src="/assets/icons/Live.svg" alt="" onClick={onPreviewClick} />
          <img src="/assets/icons/Link.svg" alt="" onClick={onLinkClick} />
          <img src="/assets/icons/QR.svg" alt="" />
        </div>
      </div>

      <div className={styles.mainLayout}>
        {selectedTemplate === 0 && <Template1 />}
        {selectedTemplate === 1 && <Template2 />}
        {selectedTemplate === 2 && <Template3 />}
        {selectedTemplate === 3 && <Template4 />}
      </div>
    </div>
  );
};

export default Layout;
