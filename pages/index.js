// dependencies
import React,{useState} from 'react'

// files import
import styles from 'styles/Home.module.scss';
import ActiveTab from 'components/ActiveTab';
import SideBarContent from 'components/SideBarContent';
import Layout from 'components/Layout';

const Home = () => {
  const [activeStep, setActiveStep] = useState(1)

  const manageCount = (count) => {
    setActiveStep(count)
  }
  return (
    <div className={styles.mainContainer}>
      <div className={styles.navigator}>
        <img src='/assets/icons/down_arrow.svg' alt='' />
        <span>
          <span>My Videos</span> / <span>Create New</span>
        </span>
      </div>
      <div className={styles.bodyContainer}>
        <div className={styles.leftContainer}>
          <span className={styles.title}>Create Video Message</span>
          <div className={styles.boxContainer}>
            <ActiveTab activeCount={activeStep} />
            <SideBarContent activeCount={activeStep} onManageCountStep={manageCount} />
          </div>
        </div>
        <div className={styles.rightContainer}>
          <Layout />
        </div>
      </div>
    </div>
  )
}

export default Home;