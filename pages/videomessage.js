// dependencies
import React, { useState, useEffect, useRef } from "react";

// files import
import styles from "styles/VideoMessage.module.scss";
import Template1 from "components/templates/Template1";
import Template2 from "components/templates/Template2";
import Template3 from "components/templates/Template3";
import Template4 from "components/templates/Template4";
import useOnScreen from "utils/hooks/useOnScreen";

const VideoMessage = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [templateData, setTemplateData] = useState({});

  const templateRef = useRef();
  const templateRefValue = useOnScreen(templateRef);
  const [selectedTemplate, setSelectedTemplate] = useState(0);

  useEffect(() => {
    setIsLoading(true);
    const params = new Proxy(new URLSearchParams(window.location.search), {
      get: (searchParams, prop) => searchParams.get(prop),
    });

    const { template, headline, desc, videourl, buttonText, isbuttonvisible, isheadlinelight } = params;
    console.log('===>', isbuttonvisible)
    const data = {
      template: Number(template),
      headline: headline,
      desc: desc,
      videourl: videourl,
      buttonText: buttonText,
      isbuttonvisible: isbuttonvisible === 'true',
      isheadlinelight: isheadlinelight === 'true'
    };

    console.log('===>',data, typeof data.isbuttonvisible)

    setSelectedTemplate(data.template)
    setTemplateData(data);
    setIsLoading(false)
  }, [templateRefValue]);

  return (
    <div className={styles.mainContainer}>
      {isLoading ? (
        <div className={styles.mainContainer}>
          <span>Loading ....</span>
        </div>
      ) : (
        <div className={styles.mainContainer} ref={templateRef}>
          {selectedTemplate === 0 && <Template1 templateData={templateData} />}
          {selectedTemplate === 1 && <Template2 templateData={templateData} />}
          {selectedTemplate === 2 && <Template3 templateData={templateData} />}
          {selectedTemplate === 3 && <Template4 templateData={templateData} />}
        </div>
      )}
    </div>
  );
};

export default VideoMessage;
