export const selectTemplateData = [
  {
    name: 'Default Design',
    img: '/assets/images/layout1.svg'
  },
  {
    name: 'Design 2',
    img: '/assets/images/layout2.svg'
  },
  {
    name: 'Design 3',
    img: '/assets/images/layout3.svg'
  },
  {
    name: 'Design 4',
    img: '/assets/images/layout4.svg'
  },
]