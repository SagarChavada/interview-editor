/* eslint-disable react-hooks/rules-of-hooks */
import { useState, useEffect } from "react";

const isSSR = typeof window === "undefined";

const EventTarget = isSSR ? Object : window.EventTarget;
export class SharedStateTarget extends EventTarget {
  constructor(initialStateOfNewComponents) {
    super();
    this.initialStateOfNewComponents = initialStateOfNewComponents;
  }

  useSharedState() {
    const [state, setState] = useState(this.initialStateOfNewComponents);
    const setSharedState = (detail) =>
      super.dispatchEvent(new CustomEvent("set", { detail }));

    useEffect(() => {
      const eventListener = ({ detail }) =>
        setState((this.initialStateOfNewComponents = detail));

      super.addEventListener("set", eventListener);
      return () => super.removeEventListener("set", eventListener);
    }, []);

    return [state, setSharedState];
  }
}

export const TEMPLATE_HEADLINE = new SharedStateTarget("Headline");
export const IS_HEADLINE_LIGHT = new SharedStateTarget(true);
export const TEMPLATE_DESC = new SharedStateTarget("Add your message here...");
export const IS_BUTTON_VISIBLE = new SharedStateTarget(true);
export const BUTTON_TEXT = new SharedStateTarget("Button text");
export const VIDEO_URL = new SharedStateTarget("");
export const SELECTED_TEMPLATE_OPTION = new SharedStateTarget(0);

